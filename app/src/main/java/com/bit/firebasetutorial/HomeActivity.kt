package com.bit.firebasetutorial

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.irozon.justbar.JustBar
import kotlinx.android.synthetic.main.activity_home.*


enum class ProviderType {
    BASIC,
    GOOGLE
}

class HomeActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Just bar
        justBar()


        // Setup
        val bundle:Bundle? = intent.extras
        val email:String? = bundle?.getString("email")
        val provider:String? = bundle?.getString("provider")
        setup(email ?: "", provider ?: "")

        // Guardado de datos

        val prefs:SharedPreferences.Editor = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        prefs.putString("email", email)
        prefs.putString("provider", provider)
        prefs.apply()

    }


    // easyslider
    private fun justBar(){
        val justBar=findViewById<JustBar>(R.id.justBarId)


        justBar.setOnBarItemClickListener { barItem, position ->
            // Your code here
            //Toast.makeText(getApplicationContext(), "Has tocado el Item en la posición: "+position, Toast.LENGTH_LONG).show()
            if(position == 0){
                Toast.makeText(getApplicationContext(), "Has tocado el Item en la opción Libros.", Toast.LENGTH_SHORT).show()
            } else if(position == 1){
                Toast.makeText(getApplicationContext(), "Has tocado el Item en la opción Buscar.", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(getApplicationContext(), "Has tocado el Item en la opción Settings", Toast.LENGTH_SHORT).show()
            }

        }

    }


    private fun setup(email: String, provider: String){

        title = "Libros"

        val usuario = "usuario: "
        val tipo = "Tipo de autentificación: "
        emailTextView.text = usuario + email
        providerTextView.text = tipo + provider

        logOutButton.setOnClickListener {

            // Borrado de datos
            val prefs:SharedPreferences.Editor = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            prefs.clear()
            prefs.apply()


            FirebaseAuth.getInstance().signOut()
            onBackPressed()
        }



        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {

            it.result?.token?.let {
                val tokenss = it
                //println("Este es el token del dispositivo: ${it}")
                println("Este es el token del dispositivo: ${tokenss}")

                saveButton.setOnClickListener {

                    db.collection("users").document(email).set(
                            hashMapOf("provider" to provider,
                                    "address" to addressTextView.text.toString(),
                                    "phone" to phoneTextView.text.toString(),
                                    "token" to tokenss.toString()
                            )

                    )
                    Toast.makeText(getApplicationContext(), "Has tocado el Boton Guardar", Toast.LENGTH_SHORT).show()

                }

            }
        }




        getButton.setOnClickListener {

            db.collection("users").document(email).get().addOnSuccessListener {
                addressTextView.setText(it.get("address") as String?)
                phoneTextView.setText(it.get("phone") as String?)
            }

            Toast.makeText(getApplicationContext(), "Has tocado el Boton recuperar", Toast.LENGTH_SHORT).show()

        }

        deleteButton.setOnClickListener {

            db.collection("users").document(email).delete()

            Toast.makeText(getApplicationContext(), "Has tocado el Boton eliminar", Toast.LENGTH_SHORT).show()

        }

        val fab = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fab.setOnClickListener { view: View? ->
            val intent = Intent(this@HomeActivity, UploadActivity::class.java)
            startActivity(intent)
        }
    }
}