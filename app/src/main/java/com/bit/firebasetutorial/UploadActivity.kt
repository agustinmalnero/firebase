package com.bit.firebasetutorial

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.activity_home.phoneTextView
import kotlinx.android.synthetic.main.activity_home.saveButton
import kotlinx.android.synthetic.main.activity_upload.*



class UploadActivity : AppCompatActivity() {

    private val File = 1
    private val database = FirebaseStorage.getInstance()
    val myRef = database.getReference("libros")

    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)

        // Setup
        val bundle:Bundle? = intent.extras
        val email:String? = bundle?.getString("email")
        val provider:String? = bundle?.getString("provider")
        setup(email ?: "", provider ?: "")

        // Guardado de datos

        val prefs:SharedPreferences.Editor = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        prefs.putString("email", email)
        prefs.putString("provider", provider)
        prefs.apply()

        imageButton.setOnClickListener {
            fileUpload()
        }

    }


    fun fileUpload() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        startActivityForResult(intent, File)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == File) {
            if (resultCode == RESULT_OK) {
                val FileUri = data!!.data
                val Folder: StorageReference =
                        FirebaseStorage.getInstance().getReference().child("Libros")
                val file_name: StorageReference = Folder.child("Portada" + FileUri!!.lastPathSegment)
                file_name.putFile(FileUri).addOnSuccessListener { taskSnapshot ->
                    file_name.getDownloadUrl().addOnSuccessListener { uri ->
                        val hashMap =
                                HashMap<String, String>()
                        hashMap["url"] = java.lang.String.valueOf(uri)

                        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {

                            it.result?.token?.let {
                                val tokenss = it
                                val emails =
                                        intent.getStringExtra("email")
                                //println("Este es el token del dispositivo: ${it}")
                                println("Este es el token del dispositivo: ${tokenss}")
                                //val prefs: SharedPreferences = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
                                // val email:String? = prefs.getString("email", null)



                                saveButton.setOnClickListener {

                                    db.collection("libros").document().set(
                                            hashMapOf("nombre" to nombreTextView.text.toString(),
                                                    "autor" to autorTextView.text.toString(),
                                                    "editorial" to editorialTextView.text.toString(),
                                                    "telefono" to phoneTextView.text.toString(),
                                                    "token" to tokenss.toString(),
                                                    "url" to hashMap.toString()
                                            )

                                    )
                                    Toast.makeText(getApplicationContext(), "Has tocado el Boton Guardar", Toast.LENGTH_SHORT).show()

                                }

                            }
                        }

                        //myRef.setValue(hashMap)
                        Log.d("Mensaje", "Se subió correctamente")
                    }
                }
            }
        }
    }


    private fun setup(email: String, provider: String){


        title = "Agregar Libro"




    }

}